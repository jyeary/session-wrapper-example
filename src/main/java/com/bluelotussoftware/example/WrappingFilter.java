/*
 * Copyright 2012 Blue Lotus Software, LLC.
 * Copyright 2012 John Yeary <jyeary@bluelotussoftware.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * $Id$
 */
package com.bluelotussoftware.example;

import com.bluelotussoftware.session.HttpSessionWrapper;
import com.bluelotussoftware.session.impl.HttpServletRequestWrapperImpl;
import java.io.IOException;
import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

/**
 * {@link Filter} used for wrapping the {@link HttpServletRequest} with an
 * implementation of the {@link HttpServletRequestWrapper} that provides a
 * custom {@link HttpSessionWrapper}.
 *
 * @author John Yeary <jyeary@bluelotussoftware.com>
 * @version 1.0
 */
@WebFilter(filterName = "WrappingFilter", urlPatterns = {"/*"},
dispatcherTypes = {
    DispatcherType.FORWARD, DispatcherType.ERROR,
    DispatcherType.REQUEST, DispatcherType.INCLUDE
})
public class WrappingFilter implements Filter {

    /**
     * Default Constructor
     */
    public WrappingFilter() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest hsr = (HttpServletRequest) request;
        HttpServletRequestWrapperImpl hsrwi = new HttpServletRequestWrapperImpl(hsr);
        chain.doFilter(hsrwi, response);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void destroy() {
        //NO-OP
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void init(FilterConfig filterConfig) {
        //NO-OP
    }
}