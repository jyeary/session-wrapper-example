/*
 * Copyright 2012 Blue Lotus Software, LLC.
 * Copyright 2012 John Yeary <jyeary@bluelotussoftware.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * $Id$
 */
package com.bluelotussoftware.example;

import com.bluelotussoftware.session.impl.HttpSessionWrapperImpl;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

/**
 *
 * @author John Yeary <jyeary@bluelotussoftware.com>
 * @version 1.0
 */
@Named
@RequestScoped
public class Index {

    public String getInfo() {
        Object o = FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        String info = null;
        if (o instanceof HttpSession) {
            HttpSession session = (HttpSession) o;

            if (session instanceof HttpSessionWrapperImpl) {
                HttpSessionWrapperImpl hswi = (HttpSessionWrapperImpl) session;
                info = hswi.INFO + "/" + hswi.getId();
            }
        }
        return info;
    }

    public List<String> getAttributeNames() {
        Object o = FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        List<String> names = null;
        if (o instanceof HttpSession) {
            HttpSession session = (HttpSession) o;

            if (session instanceof HttpSessionWrapperImpl) {
                names = ((HttpSessionWrapperImpl) session).getAttributesNames();
            }
        }
        return names;
    }
}
